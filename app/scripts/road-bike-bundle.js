
// Invodo Init - required for Invodo.js to function properly
Invodo.init({
  pageName: 'Road Bike Bundle',
  pageType: 'product',
//  debugLogger: 'true'
});


/*
 * Initialize Shoppable Video
 */
var ivp = new Invodo.Ixd.Ivp(document.getElementById('gopro-ivp-widget'), {
  refId: 'H8PRFZE5',
  theme: 'ivp-gopro-theme',
  name: 'road-bike-bundle',
  dataPath: 'data.json',
  /*
   * Example callbacks. Please replace with actual implementations!
   */
    callbacks: {
    /*
     * Argument is a string identifier for a product. Returns true on success and
     * and false on failure.
     */
    addToCart: function(product) {
      alert('Adding ' + product + ' to cart!');
      return true;
    },
    /*
     * Argument is a string identifier for a product. Returns localized string.
     */
    getProductPrice: function(bundle) {
      var price = 'N/A';
      switch(bundle) {
        case 'ROAD_BIKE_BUNDLE':
          price = 'US $499.00';
          break;
      }
      return price;
    },
    /*
     * Argument is a string identifier for a product. Returns localized string.
     */
    getProductItemPrice: function(item) {
      var price = 'N/A';
      switch(item) {
        case 'VHS':
          price = 'US $14.99';
          break;
        case 'HSP':
          price = 'US $19.99';
          break;
        case 'TheStrap':
          price = 'US $59.99';
          break;
        case 'HERO4_session':
          price = 'US $199.99';
          break;
      }
      return price;
    },
    /*
     * Is the Float bundle in the cart? Returns a boolean
     */
    isBundleInCart: function() {
      return false;
    }
  }
},
/*
 * You should not need to edit anything below this line.
 */
function() {
  var callbacks = this.settings().callbacks;

  // Is the bundle in the cart on page load?
  var bundleInCart = callbacks.isBundleInCart();

  // Updates document elements to display cart status.
  function updateCartStatus() {
    var elems = document.getElementsByClassName('ivp-gopro-cart-status');
    Array.prototype.forEach.call(elems, function(el) {
      if(!bundleInCart) {
        el.style.display = 'none';
      } else {
        el.style.display = 'inline-block';
      }
    });
  }
  updateCartStatus();

  // Add to cart click handlers.
  var elems = document.getElementsByClassName('ivp-add-to-cart');
  Array.prototype.forEach.call(elems, function(el) {
    el.addEventListener('click', function(e) {
      var item = el.getAttribute('data-ivp-item');
      callbacks.addToCart(item);
      event.stopPropagation();
      e.preventDefault();
    });
  });

  // Add to cart click handlers.
  elems = document.getElementsByClassName('ivp-card-cta--primary');
  Array.prototype.forEach.call(elems, function(el) {
    el.addEventListener('click', function(e) {
      if(callbacks.addToCart('ROAD_BIKE_BUNDLE')) {
        bundleInCart = true;
      }
      updateCartStatus();
      e.preventDefault();
    });
  });

  // Retrieve dynamic prices.
  elems = document.getElementsByClassName('ivp-gopro-card-price');
  Array.prototype.forEach.call(elems, function(el) {
    el.textContent = callbacks.getProductPrice('ROAD_BIKE_BUNDLE');
  });

  // Retrieve dynamic prices.
  elems = document.getElementsByClassName('ivp-gopro-card-item-price');
  Array.prototype.forEach.call(elems, function(el) {
    var productItem = el.getAttribute('data-ivp-item');
    el.textContent = callbacks.getProductItemPrice(productItem);
  });

  // Set the poster image as the ivp-poster background
  var poster = this._node.querySelector('.ivp-poster');
  poster.style.backgroundImage = 'url(' + this.settings().podConfig.frames.items[0].poster + ')';

  var video = this._node.querySelector('video');
  // Blur the video
  this.observer.on('card.show', function() {
  });
  // Un-blur the video
  this.observer.on('card.hide', function() {
  });
  // Show recap card when video ends
  this.observer.on('ended', function() {
    this.card.enable(0);
  });
});
